﻿using System.Collections.Generic;

public class Selector : Node
{
    protected readonly List<Node> nodes;

    public Selector(List<Node> nodes)
    {
        this.nodes = nodes;
    }

    public override NodeState Evaluate()
    {
        foreach (Node node in nodes)
        {
            switch (node.Evaluate())
            {
                case NodeState.Running:
                    NodeState = NodeState.Running;
                    return NodeState;
                case NodeState.Success:
                    NodeState = NodeState.Success;
                    return NodeState;
                case NodeState.Failure:
                    break;
            }
        }

        NodeState = NodeState.Failure;
        return NodeState;
    }
}