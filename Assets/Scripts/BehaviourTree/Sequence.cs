﻿using System.Collections.Generic;

public class Sequence : Node
{
    protected readonly List<Node> nodes;

    public Sequence(List<Node> nodes)
    {
        this.nodes = nodes;
    }

    public override NodeState Evaluate()
    {
        bool isAnyNodeRunning = false;
        foreach (Node node in nodes)
        {
            switch (node.Evaluate())
            {
                case NodeState.Running:
                    isAnyNodeRunning = true;
                    break;
                case NodeState.Failure:
                    NodeState = NodeState.Failure;
                    return NodeState;
                case NodeState.Success:
                    break;
            }
        }

        NodeState = isAnyNodeRunning
            ? NodeState.Running
            : NodeState.Success;

        return NodeState;
    }
}