﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class Node : MonoBehaviour
{
    public NodeState NodeState { get; protected set; }

    public abstract NodeState Evaluate();
}