﻿using System;

public class Invertor : Node
{
    protected Node node;

    
    public Invertor(Node node)
    {
        this.node = node;
    }

    
    public override NodeState Evaluate()
    {
        switch (node.Evaluate())
        {
            case NodeState.Running:
                NodeState = NodeState.Running;
                break;
            case NodeState.Success:
                NodeState = NodeState.Failure;
                break;
            case NodeState.Failure:
                NodeState = NodeState.Success;
                break;
        }

        return NodeState;
    }
}