﻿using UnityEngine;

namespace DefaultNamespace
{
    public class Cover : MonoBehaviour
    {
        public Transform[] CoverSpots => coverSpots;
        
        [SerializeField] private Transform[] coverSpots;
    }
}