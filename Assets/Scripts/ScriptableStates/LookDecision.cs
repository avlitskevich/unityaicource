﻿using UnityEngine;

namespace ScriptableStates
{
    [CreateAssetMenu(menuName = "ScriptableStates/Decisions/Look")]
    public class LookDecision : Decision
    {
        public float lookDistance;
        
        public override bool Decide(EnemyController controller)
        {
            RaycastHit hit;

            if (Physics.Raycast(controller.transform.position, controller.transform.forward, out hit,
                lookDistance))
            {
                if (hit.collider.GetComponent<PlayerScript>() != null)
                    return true;
            }

            return false;
        }
    }
}