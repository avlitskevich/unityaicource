﻿using UnityEngine;

namespace ScriptableStates
{
    [CreateAssetMenu(menuName = "ScriptableStates/Actions/FollowPlayer")]
    public class FollowPlayerAction : Action
    {
        public override void Act(EnemyController controller)
        {
            if (controller.animator.GetBool("Attack"))
                controller.animator.SetBool("Attack", false);
            
            controller.navMeshAgent.SetDestination(controller.player.position);
        }
    }
}