﻿using UnityEngine;

namespace ScriptableStates
{
    [CreateAssetMenu(menuName = "ScriptableStates/Actions/Waypoints")]
    public class WaypointsActions : Action
    {
        public float changeWaypointDistance;
        
        
        public override void Act(EnemyController controller)
        {
            if (CheckDistanceToCurrentWaypoint(controller))
                controller.currentWaypoint = (controller.currentWaypoint + 1) % controller.waypoints.Length;
            
            controller.navMeshAgent.SetDestination(controller.CurrentWaypointPosition);
        }

        
        private bool CheckDistanceToCurrentWaypoint(EnemyController controller)
        {
            Vector3 nextWaypointDirection =
                controller.transform.position - controller.CurrentWaypointPosition;

            float dist = nextWaypointDirection.magnitude;

            return dist < changeWaypointDistance;
        }
    }
}