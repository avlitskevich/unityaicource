﻿using UnityEngine;

namespace ScriptableStates
{
    [CreateAssetMenu(menuName = "ScriptableStates/Decisions/Distance")]
    public class DistanceDecision : Decision
    {
        public float distance;
        
        public override bool Decide(EnemyController controller)
        {
            Vector3 enemyToPlayerDir = controller.transform.position - controller.player.position;
            float dist = enemyToPlayerDir.magnitude;
            
            return dist < distance;
        }
    }
}