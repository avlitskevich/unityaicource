﻿using UnityEngine;

namespace ScriptableStates
{
    [CreateAssetMenu(menuName = "ScriptableStates/Actions/Attack")]
    public class AttackPlayerAction : Action
    {
        public override void Act(EnemyController controller)
        {
            if (!controller.animator.GetBool("Attack"))
                controller.animator.SetBool("Attack", true);
        }
    }
}