﻿using UnityEngine;

namespace ScriptableStates
{
    [CreateAssetMenu(menuName = "ScriptableStates/State")]
    public class State : ScriptableObject
    {
        public Action[] actions;
        public Transition[] transitions;
        

        public void UpdateState(EnemyController controller)
        {
            foreach (Action action in actions)
                action.Act(controller);

            CheckTransitions(controller);
        }


        private void CheckTransitions(EnemyController controller)
        {
            foreach (Transition transition in transitions)
                if (transition.decision.Decide(controller) != transition.invertDecision)
                    controller.ChangeState(transition.nextState);
        }
    }
}