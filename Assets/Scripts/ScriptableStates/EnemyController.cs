﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

namespace ScriptableStates
{
    class TriggerSceneContainer : MonoBehaviour
    {
        public int sceneIndex;
        public bool load;
    }
    
    public class EnemyController : MonoBehaviour
    {
        public Vector3 CurrentWaypointPosition => waypoints[currentWaypoint].position;
        
        public int currentWaypoint;
        
        public State currentState;
        public Transform player;
        public Animator animator;
        public NavMeshAgent navMeshAgent;
        public Transform[] waypoints;
        
        


        private void Update()
        {
            currentState.UpdateState(this);
        }

        private void OnTriggerEnter(Collider other)
        {
            TriggerSceneContainer container = other.GetComponent<TriggerSceneContainer>();
            if (container != null)
            {
                if (container.load)
                    SceneManager.LoadScene(container.sceneIndex, LoadSceneMode.Additive);
                else
                    SceneManager.UnloadSceneAsync(container.sceneIndex);
            }
        }


        public void ChangeState(State nextState)
        {
            if (nextState == null || nextState == currentState)
                return;

            currentState = nextState;
        }

        [ContextMenu("LoadScene")]
        public void LoadScene2Additive()
        {
            SceneManager.LoadScene(1, LoadSceneMode.Additive);
        }

        [ContextMenu("UnloadScene")]
        public void UnloadScene()
        {
            AsyncOperation unloadingScene = SceneManager.UnloadSceneAsync(1);
            StartCoroutine(UnloadingScene(unloadingScene));
        }

        private IEnumerator UnloadingScene(AsyncOperation unloading)
        {
            while (!unloading.isDone)
            {
                Debug.Log(unloading.progress);
                yield return null;
            }
        }
    }
}