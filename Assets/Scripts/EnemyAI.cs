﻿using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    public float CurrentHealth
    {
        get => currentHealth;
        set => currentHealth = Mathf.Clamp(value, 0, baseHealth);
    }
    public Color Color
    {
        set => material.color = value;
    }
    
    [SerializeField] private float baseHealth;
    [SerializeField] private float lowHealthThreshold;
    [SerializeField] private float healthRestoreValue;
    [SerializeField] private float shootRange;
    [SerializeField] private float chaseRange;

    private float currentHealth;
    private Material material;


    private void Start()
    {
        CurrentHealth = baseHealth;
        material = GetComponent<MeshRenderer>().material;
    }

    private void Update()
    {
        currentHealth += healthRestoreValue * Time.deltaTime;
    }
}