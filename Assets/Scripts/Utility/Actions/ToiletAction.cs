﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToiletAction : Action
{
    public override float CalculateUtility(Character character)
    {
        float value = evaluator.Evaluate(character.Characteristic.urine);
        character.Calculated.urine = value;
        return value;
    }

    public override void Act(Character character)
    {
        base.Act(character);
        
        if (IsMovingToTargetPosition(character))
            return;

        character.Characteristic.Urine += urineEffect;
        character.Cooldown = cooldown;
    }
}