﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SleepAction : Action
{
    public override float CalculateUtility(Character character)
    {
        float value = evaluator.Evaluate(character.Characteristic.sleep);
        character.Calculated.sleep = value;
        return value;
    }

    public override void Act(Character character)
    {
        base.Act(character);
        
        if (IsMovingToTargetPosition(character))
            return;

        character.Characteristic.Energy += energyEffect;
        character.Characteristic.Sleep += sleepEffect;
        character.Cooldown = cooldown;
    }
}