﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EatAction : Action
{
    public override float CalculateUtility(Character character)
    {
        float value = evaluator.Evaluate(character.Characteristic.hunger);
        character.Calculated.hunger = value;
        return value;
    }

    public override void Act(Character character)
    {
        base.Act(character);
        
        if (IsMovingToTargetPosition(character))
            return;

        character.Characteristic.Hunger += hungerEffect;
        character.Characteristic.Energy += energyEffect;
        character.Characteristic.Urine += urineEffect;
        character.Cooldown = cooldown;
    }
}