﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkAction : Action
{
    public override float CalculateUtility(Character character)
    {
        float value = evaluator.Evaluate(character.Characteristic.energy);
        character.Calculated.energy = value;
        return value;
    }

    public override void Act(Character character)
    {
        base.Act(character);
        
        if (IsMovingToTargetPosition(character))
            return;

        character.Characteristic.Hunger += hungerEffect;
        character.Characteristic.Energy += energyEffect;
        character.Characteristic.Urine += urineEffect;
        character.Characteristic.Sleep += sleepEffect;
        character.Cooldown = cooldown;
    }
}