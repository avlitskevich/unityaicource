﻿using System;
using UnityEngine;

public abstract class Action : MonoBehaviour
{
    [SerializeField] protected float hungerEffect;
    [SerializeField] protected float energyEffect;
    [SerializeField] protected float urineEffect;
    [SerializeField] protected float sleepEffect;
    
    [SerializeField] private float distanceToAct;
    [SerializeField] protected float cooldown;
    [SerializeField] private Color color;
    [SerializeField] private Renderer renderer;
    [SerializeField] protected AnimationCurve evaluator;


    private void Start()
    {
        renderer.material.color = color;
    }


    public abstract float CalculateUtility(Character character);

    public virtual void Act(Character character)
    {
        character.Color = color;
    }


    protected bool IsMovingToTargetPosition(Character character)
    {
        if (Vector3.Distance(character.transform.position, transform.position) < distanceToAct)
            return false;
        
        character.Agent.SetDestination(transform.position);
        return true;
    }
}