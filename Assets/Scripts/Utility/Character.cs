﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class Character : MonoBehaviour
{
    public float Cooldown
    {
        set => cooldownTimer = value;
    }
    public Color Color
    {
        set => renderer.material.color = value;
    }
    public NavMeshAgent Agent => agent;
    public Characteristics Characteristic => characteristics;
    public Characteristics Calculated => calculated;

    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private Renderer renderer;
    [SerializeField] private Characteristics characteristics;
    [SerializeField] private Characteristics calculated;
    [SerializeField] private Action[] actions;

    private float cooldownTimer;
    
    

    private void Update()
    {
        if (cooldownTimer > 0)
        {
            cooldownTimer -= Time.deltaTime;
            return;
        }
        
        if (actions == null || actions.Length == 0)
            return;

        actions.OrderByDescending(x => x.CalculateUtility(this))
            .First()
            .Act(this);
    }
}