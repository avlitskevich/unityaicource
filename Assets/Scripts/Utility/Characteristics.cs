﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Characteristics
{
    public float Energy
    {
        get => energy;
        set => energy = Mathf.Clamp01(value);
    }
    public float Hunger
    {
        get => hunger;
        set => hunger = Mathf.Clamp01(value);
    }
    public float Sleep
    {
        get => sleep;
        set => sleep = Mathf.Clamp01(value);
    }
    public float Urine
    {
        get => urine;
        set => urine = Mathf.Clamp01(value);
    }
    
    public float energy;
    public float hunger;
    public float sleep;
    public float urine;
}