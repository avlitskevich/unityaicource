﻿using DefaultNamespace;
using UnityEngine;

public class IsCoverAvailableNode : Node
{
    private Transform target;
    private EnemyAI enemy;
    private Cover[] availableCovers;

    
    public IsCoverAvailableNode(Transform target, EnemyAI enemy, Cover[] availableCovers)
    {
        this.target = target;
        this.enemy = enemy;
        this.availableCovers = availableCovers;
    }


    public override NodeState Evaluate()
    {
        Transform bestSpot = FindBestCoverSpot();

        return bestSpot == null
            ? NodeState.Success
            : NodeState.Failure;
    }


    private Transform FindBestCoverSpot()
    {
        float minAngle = 90;
        Transform bestSpot = null;

        foreach (Cover cover in availableCovers)
        {
            Transform bestSpotInCover = FindBestSpotInCover(cover, ref minAngle);
            if (bestSpotInCover != null)
                bestSpot = bestSpotInCover;
        }

        return bestSpot;
    }

    private Transform FindBestSpotInCover(Cover cover, ref float minAngle)
    {
        Transform[] availableSpots = cover.CoverSpots;
        return null;
    }
}