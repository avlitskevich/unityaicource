﻿using UnityEngine;
using UnityEngine.AI;

public class ShootNode : Node
{
    private EnemyAI enemy;
    private NavMeshAgent agent;

    
    public ShootNode(EnemyAI enemy, NavMeshAgent agent)
    {
        this.enemy = enemy;
        this.agent = agent;
    }

    
    public override NodeState Evaluate()
    {
        agent.isStopped = true;
        enemy.Color = Color.green;
        return NodeState.Running;
    }
}