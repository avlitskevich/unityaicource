﻿using UnityEngine;
using UnityEngine.AI;

public class ChaseNode : Node
{
    private const float StopDistance = 0.2f;
    
    private Transform target;
    private NavMeshAgent agent;
    private EnemyAI enemy;

    
    public ChaseNode(Transform target, NavMeshAgent agent, EnemyAI enemy)
    {
        this.target = target;
        this.agent = agent;
        this.enemy = enemy;
    }

    
    public override NodeState Evaluate()
    {
        enemy.Color = Color.gray;
        float distance = Vector3.Distance(target.position, agent.transform.position);

        if (distance > StopDistance)
        {
            agent.isStopped = false;
            agent.SetDestination(target.position);
            return NodeState.Running;
        }

        agent.isStopped = true;
        return NodeState.Success;
    }
}