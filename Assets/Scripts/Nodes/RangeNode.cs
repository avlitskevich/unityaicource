﻿using UnityEngine;

public class RangeNode : Node
{
    private float range;
    private Transform target;
    private Transform self;

    
    public RangeNode(float range, Transform target, Transform self)
    {
        this.range = range;
        this.target = target;
        this.self = self;
    }

    
    public override NodeState Evaluate()
    {
        float distance = Vector3.Distance(target.position, self.position);
        
        return distance <= range
            ? NodeState.Success
            : NodeState.Failure;
    }
}