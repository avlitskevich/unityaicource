﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthNode : Node
{
    private EnemyAI enemy;
    private float threshold;

    
    public HealthNode(EnemyAI enemy, float threshold)
    {
        this.enemy = enemy;
        this.threshold = threshold;
    }

    
    public override NodeState Evaluate()
    {
        return enemy.CurrentHealth <= threshold
            ? NodeState.Success
            : NodeState.Failure;
    }
}