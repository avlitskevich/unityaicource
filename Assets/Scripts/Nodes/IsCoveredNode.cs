﻿using UnityEngine;

public class IsCoveredNode : Node
{
    private Transform target;
    private Transform origin;


    public IsCoveredNode(Transform target, Transform origin)
    {
        this.target = target;
        this.origin = origin;
    }


    public override NodeState Evaluate()
    {
        if (Physics.Raycast(
                origin.position,
                target.position - origin.position,
                out RaycastHit hit)
            && hit.collider.transform != target)
        {
            return NodeState.Success;
        }

        return NodeState.Failure;
    }
}